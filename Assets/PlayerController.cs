﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    Animator animator;
    Transform player;
    public float moveSpeed = 0.3f;

    private bool Timer;

    public float speed = 0;
    void Start()
    {
        animator = GetComponent<Animator>();
        player = GetComponent<Transform>();

    }

    void Update()
    {
        float horMovement = Input.GetAxisRaw("Horizontal");
        float vertMovement = Input.GetAxisRaw("Vertical");
        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            animator.SetBool("walk", true);
            animator.SetBool("run", true);
            
        }
        if (Input.GetKeyDown(KeyCode.W) | Input.GetKeyDown(KeyCode.S) | Input.GetKeyDown(KeyCode.A) | Input.GetKeyDown(KeyCode.D))
        {
            animator.SetBool("walk", true);
        }
        else if (Input.GetKeyUp(KeyCode.W) | Input.GetKeyUp(KeyCode.S) | Input.GetKeyUp(KeyCode.A) | Input.GetKeyUp(KeyCode.D))
        {
            animator.SetBool("walk", false);
        } // Чекнуть тут 
        else if (Input.GetKeyDown(KeyCode.Space) & Input.GetAxisRaw("Horizontal") == 0 & Input.GetAxisRaw("Vertical") == 0)
        {
            animator.SetBool("jump", true);
            animator.Play("Jump");
        }
        else if(!Input.GetKeyDown(KeyCode.Space))
            animator.SetBool("jump", false);
        

        if (Input.GetKeyUp(KeyCode.LeftShift))
        {

            animator.SetBool("run", false);
            animator.SetBool("walk", false);

        }


       
       
        

        Movement();


        // Движение вправо и влевод

    }
    

    

    void Movement()
    {


        float horMovement = Input.GetAxisRaw("Horizontal");
        float vertMovement = Input.GetAxisRaw("Vertical");
        transform.Translate(transform.right * horMovement * Time.deltaTime * moveSpeed);
        transform.Translate(transform.forward * vertMovement * Time.deltaTime );

        Vector3 moveDirection = new Vector3(horMovement, 0, vertMovement);
        if (moveDirection != Vector3.zero)
        {
            Quaternion newRotation = Quaternion.LookRotation(moveDirection);
            player.transform.rotation = Quaternion.Slerp(player.transform.rotation, newRotation, Time.deltaTime * 8);
        }
        
    }
}
